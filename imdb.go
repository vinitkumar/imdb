package main

import "encoding/json"
import "fmt"

type Movie struct {
	Popularity float32
	Director   string
	Genre      [5]string
	Score      float32
	Name       string
}

func main() {
	m := Movie{
		83.0,
		"victor flaming",
		[5]string{"horror", "suspense"},
		8.3,
		"The Wizard of Oz",
	}
	b, err := json.Marshal(m)

	if err != nil {
		panic(err)
	} else {
		fmt.Println(b)
	}

	var movie Movie

	error := json.Unmarshal(b, &movie)
	if error != nil {
		panic(error)
	} else {
		fmt.Println(movie)
	}
}
