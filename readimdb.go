package main

import "fmt"
import "os"
import "encoding/json"

type Movie struct {
	Popularity float32   `json:"99popularity"`
	Director   string    `json:"director"`
	Genre      [5]string `json:"genre"`
	Score      float32   `json: "imdb_score"`
	Name       string    `json: "name"`
}

func ReadFromJSON(jsonfile string) {
	jsonFile, err := os.Open(jsonfile)
	if err != nil {
		panic(err)
	}

	var moviearray []Movie
	jsonParser := json.NewDecoder(jsonFile)
	if err = jsonParser.Decode(&moviearray); err != nil {
		panic(err)
	}

	for index, movie := range moviearray {
		fmt.Println(index, movie)
	}
}

func main() {
	ReadFromJSON("imdb.json")
}
