# IMDB

A sample read example for reading json and decoding using structs.

`imdb.go` just parses a json object and prints it, where as `readimdb.go` parses
the json file and prints it.
